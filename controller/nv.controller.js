function layThongTinTuForm() {
  const tkNv = document.getElementById("tknv").value;

  const tenNv = document.getElementById("name").value;

  const emailNv = document.getElementById("email").value;

  const mkNv = document.getElementById("password").value;

  const ngayLamNv = document.getElementById("datepicker").value;

  const luongNv = document.getElementById("luongCB").value;

  const chucVuNv = document.getElementById("chucvu").value;

  const gioLamNv = document.getElementById("gioLam").value;

  return new NhanVien(
    tkNv,
    tenNv,
    emailNv,
    mkNv,
    ngayLamNv,
    luongNv,
    chucVuNv,
    gioLamNv
  );
}
// Render DSNV
function renderDSNV(nvArr) {
  // contentHTML : chuỗi chứa các thẻ <tr></tr>;
  var contentHTML = "";
  for (var i = 0; i < nvArr.length; i++) {
    var nv = nvArr[i];
    // trContent thẻ tr trong mỗi lần lặp
    var trContent = ` <tr>
      <td>${nv.taiKhoan}</td>
      <td>${nv.ten}</td>
      <td>${nv.email}</td>
      <td>${nv.ngayLam}</td>
      <td>${nv.chucVu}</td>
      <td>${nv.tinhTongLuong()}</td>
      <td>${nv.tinhXepLoai()}</td>
      <td>
      <button onclick="xoaNhanVien('${
        nv.taiKhoan
      }')" class="btn btn-danger">Xoá</button>
      <button onclick="suaNhanVien('${nv.taiKhoan}')"
      class="btn btn-warning" id="btnThem" data-toggle="modal" data-target="#myModal">Sửa</button>
      </td>
      </tr>`;
    contentHTML += trContent;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

function timKiemViTri(id, dsnv) {
  for (var index = 0; index < dsnv.length; index++) {
    var nv = dsnv[index];
    if (nv.taiKhoan == id) {
      return index;
    }
  }
  //  ko tìm thấy
  return -1;
}

function timKiemViTriTheoXepLoai(xepLoai, dsnv) {
  var newDSNV = [];
  for (var index = 0; index < dsnv.length; index++) {
    var nv = dsnv[index];
    if (nv.tinhXepLoai() == xepLoai) {
      newDSNV.push(nv);
    }
  }
  return newDSNV;
}

function showThongTinLenForm(nv) {
  document.getElementById("tknv").value = nv.taiKhoan;
  document.getElementById("name").value = nv.ten;
  document.getElementById("email").value = nv.email;
  document.getElementById("password").value = nv.matKhau;
  document.getElementById("datepicker").value = nv.ngayLam;
  document.getElementById("luongCB").value = nv.luongCoBan;
  document.getElementById("chucvu").value = nv.chucVu;
  document.getElementById("gioLam").value = nv.gioLam;
}
