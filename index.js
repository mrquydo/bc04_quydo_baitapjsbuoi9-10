const DSNV_LOCALSTORAGE = "DSNV_LOCALSTORAGE";

var dsnvJson = localStorage.getItem(DSNV_LOCALSTORAGE);

var dsnv = [];

if (dsnvJson != null) {
  console.log("dsnvJson: ", dsnvJson);
  dsnv = JSON.parse(dsnvJson);
  //  array khi convert thành json sẽ mất function, ta sẽ map lại
  for (var index = 0; index < dsnv.length; index++) {
    var nv = dsnv[index];
    dsnv[index] = new NhanVien(
      nv.taiKhoan,
      nv.ten,
      nv.email,
      nv.matKhau,
      nv.ngayLam,
      nv.luongCoBan,
      nv.chucVu,
      nv.gioLam
    );
  }

  renderDSNV(dsnv);
}

function themNV() {
  var newNv = layThongTinTuForm();
  var isValidTaiKhoan =
    validation.kiemTraRong(
      newNv.taiKhoan,
      "tbTKNV",
      "vui lòng điền thông tin"
    ) &&
    validation.kiemTraDoDai(newNv.taiKhoan, "tbTKNV", "Nhập 4-6 ký số", 4, 6) &&
    validation.kiemTraSo(newNv.taiKhoan, "tbTKNV", "Nhập 4-6 ký số");

  var isValidTen =
    validation.kiemTraRong(newNv.ten, "tbTen", "Vui lòng điền thông tin") &&
    validation.kiemTraChu(newNv.ten, "tbTen", "Chỉ nhập ký tự");

  var isValidEmail =
    validation.kiemTraRong(
      newNv.email,
      "tbEmail",
      "Email khong duoc de rong"
    ) && validation.kiemTraEmail(newNv.email, "tbEmail", "Email khong dung");

  var isValidMatKhau =
    validation.kiemTraRong(
      newNv.matKhau,
      "tbMatKhau",
      "Vui lòng điền thông tin"
    ) &&
    validation.kiemTraPassword(
      newNv.matKhau,
      "tbMatKhau",
      "At least 1 number, 1 uppercase, 1 special symbol"
    );

  var isValidNgayLam =
    validation.kiemTraRong(
      newNv.ngayLam,
      "tbNgay",
      "Vui lòng điền thông tin"
    ) &&
    validation.kiemTraNgayThang(
      newNv.ngayLam,
      "tbNgay",
      "Ngày tháng sai định dạng"
    );

  var isValidLuong =
    validation.kiemTraRong(
      newNv.luongCoBan,
      "tbLuongCB",
      "Vui lòng điền thông tin"
    ) &&
    validation.kiemTraLuong(
      newNv.luongCoBan,
      "tbLuongCB",
      "Nhập trong khoảng 1 000 000 - 20 000 000"
    );

  var isValidChucVu = validation.kiemTraChucVu(
    newNv.chucVu,
    "tbChucVu",
    "Sai chức vụ"
  );

  var isValidGioLam =
    validation.kiemTraRong(
      newNv.gioLam,
      "tbGiolam",
      "Vui lòng điền thông tin"
    ) &&
    validation.kiemTraGioLam(
      newNv.gioLam,
      "tbGiolam",
      "Nhập giờ làm 80 - 200 giờ"
    );

  if (
    isValidTaiKhoan &&
    isValidEmail &&
    isValidTen &&
    isValidMatKhau &&
    isValidNgayLam &&
    isValidLuong &&
    isValidChucVu &&
    isValidGioLam
  ) {
    // var dsnv = [];
    dsnv.push(newNv);
    // tạo json
    var dsnvJson = JSON.stringify(dsnv);
    console.log("dsnvJson: ", dsnvJson);

    // lưu json vào localStorage
    localStorage.setItem(DSNV_LOCALSTORAGE, dsnvJson);
    renderDSNV(dsnv);
  }
}

function xoaNhanVien(id) {
  var index = timKiemViTri(id, dsnv);
  // tìm thấy vị trí
  if (index != -1) {
    dsnv.splice(index, 1);
    renderDSNV(dsnv);
  }
}

function suaNhanVien(id) {
  var index = timKiemViTri(id, dsnv);
  if (index != -1) {
    var nv = dsnv[index];

    // document.body.classList.toggle("modal-open");
    // document.getElementById("myModal").classList.toggle("show");
    showThongTinLenForm(nv);
  }
  document.getElementById("tknv").setAttribute("disabled", "");
}

function capNhatNhanVien() {
  var id = document.getElementById("tknv").value;
  var index = timKiemViTri(id, dsnv);
  var nv = dsnv[index];
  nv.ten = document.getElementById("name").value;
  nv.email = document.getElementById("email").value;
  nv.matKhau = document.getElementById("password").value;
  nv.ngayLam = document.getElementById("datepicker").value;
  nv.luongCoBan = document.getElementById("luongCB").value;
  nv.chucVu = document.getElementById("chucvu").value;
  nv.gioLam = document.getElementById("gioLam").value;

  var isValidTaiKhoan =
    validation.kiemTraRong(nv.taiKhoan, "tbTKNV", "vui lòng điền thông tin") &&
    validation.kiemTraDoDai(nv.taiKhoan, "tbTKNV", "Nhập 4-6 ký số", 4, 6) &&
    validation.kiemTraSo(nv.taiKhoan, "tbTKNV", "Nhập 4-6 ký số");

  var isValidTen =
    validation.kiemTraRong(nv.ten, "tbTen", "Vui lòng điền thông tin") &&
    validation.kiemTraChu(nv.ten, "tbTen", "Chỉ nhập ký tự");

  var isValidEmail =
    validation.kiemTraRong(nv.email, "tbEmail", "Email khong duoc de rong") &&
    validation.kiemTraEmail(nv.email, "tbEmail", "Email khong dung");

  var isValidMatKhau =
    validation.kiemTraRong(
      nv.matKhau,
      "tbMatKhau",
      "Vui lòng điền thông tin"
    ) &&
    validation.kiemTraPassword(
      nv.matKhau,
      "tbMatKhau",
      "At least 1 number, 1 uppercase, 1 special symbol"
    );

  var isValidNgayLam =
    validation.kiemTraRong(nv.ngayLam, "tbNgay", "Vui lòng điền thông tin") &&
    validation.kiemTraNgayThang(
      nv.ngayLam,
      "tbNgay",
      "Ngày tháng sai định dạng"
    );

  var isValidLuong =
    validation.kiemTraRong(
      nv.luongCoBan,
      "tbLuongCB",
      "Vui lòng điền thông tin"
    ) &&
    validation.kiemTraLuong(
      nv.luongCoBan,
      "tbLuongCB",
      "Nhập trong khoảng 1 000 000 - 20 000 000"
    );

  var isValidChucVu = validation.kiemTraChucVu(
    nv.chucVu,
    "tbChucVu",
    "Sai chức vụ"
  );

  var isValidGioLam =
    validation.kiemTraRong(nv.gioLam, "tbGiolam", "Vui lòng điền thông tin") &&
    validation.kiemTraGioLam(
      nv.gioLam,
      "tbGiolam",
      "Nhập giờ làm 80 - 200 giờ"
    );
  if (
    isValidTaiKhoan &&
    isValidEmail &&
    isValidTen &&
    isValidMatKhau &&
    isValidNgayLam &&
    isValidLuong &&
    isValidChucVu &&
    isValidGioLam
  ) {
    renderDSNV(dsnv);
  }
}

function searchNhanVien() {
  var xepLoai = document.getElementById("searchName").value;
  var newDSNV = timKiemViTriTheoXepLoai(xepLoai, dsnv);
  renderDSNV(newDSNV);
}
