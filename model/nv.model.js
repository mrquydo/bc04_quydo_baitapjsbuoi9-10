function NhanVien(
  taiKhoan,
  ten,
  email,
  matKhau,
  ngayLam,
  luongCoBan,
  chucVu,
  gioLam
) {
  this.taiKhoan = taiKhoan;
  this.ten = ten;
  this.email = email;
  this.matKhau = matKhau;
  this.ngayLam = ngayLam;
  this.luongCoBan = luongCoBan;
  this.chucVu = chucVu;
  this.gioLam = gioLam;
  this.tinhTongLuong = function () {
    if (this.chucVu == "Sếp") {
      return this.luongCoBan * 1 * 3;
    }
    if (this.chucVu == "Trưởng phòng") {
      return this.luongCoBan * 1 * 2;
    } else {
      return this.luongCoBan * 1 * 1;
    }
  };
  this.tinhXepLoai = function () {
    if (this.gioLam >= 192) {
      return "Xuất Sắc";
    }
    if (this.gioLam >= 176) {
      return "Giỏi";
    }
    if (this.gioLam >= 160) {
      return "Khá";
    } else {
      return "Trung Bình";
    }
  };
}
