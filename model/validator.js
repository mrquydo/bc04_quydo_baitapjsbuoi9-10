var validation = {
  kiemTraRong: function (value, idError, message) {
    if (value.length == 0) {
      document.getElementById(idError).innerText = message;
      document.getElementById(idError).style.display = "block";
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },
  kiemTraDoDai: function (value, idError, message, min, max) {
    if (value.length < min || value.length > max) {
      document.getElementById(idError).innerText = message;
      document.getElementById(idError).style.display = "block";
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },
  kiemTraEmail: function (value, idError, message) {
    const re =
      /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

    if (re.test(value)) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).innerText = message;
      document.getElementById(idError).style.display = "block";
      return false;
    }
  },

  kiemTraSo: function (value, idError, message) {
    if (isNaN(value)) {
      document.getElementById(idError).innerText = message;
      document.getElementById(idError).style.display = "block";
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  },

  kiemTraChu: function (value, idError, message) {
    if (isNaN(value)) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).innerText = message;
      document.getElementById(idError).style.display = "block";
      return false;
    }
  },
  kiemTraPassword: function (value, idError, message) {
    const passRegex =
      /^(?!.*\s)(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[~`!@#$%^&*()--+={}\[\]|\\:;"'<>,.?/_₹]).{6,10}$/;
    if (passRegex.test(value)) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).innerText = message;
      document.getElementById(idError).style.display = "block";
      return false;
    }
  },

  kiemTraNgayThang: function (value, idError, message) {
    const dateformat =
      /^((0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.](19|20)?[0-9]{2})*$/;

    if (dateformat.test(value)) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).innerText = message;
      document.getElementById(idError).style.display = "block";
      return false;
    }
  },
  kiemTraLuong: function (value, idError, message) {
    if (value >= 1000000 && value <= 20000000) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).innerText = message;
      document.getElementById(idError).style.display = "block";
      return false;
    }
  },
  kiemTraChucVu: function (value, idError, message) {
    if (value == "Sếp" || value == "Trưởng phòng" || value == "Nhân viên") {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).innerText = message;
      document.getElementById(idError).style.display = "block";
      return false;
    }
  },
  kiemTraGioLam: function (value, idError, message) {
    if (value >= 80 && value <= 200) {
      document.getElementById(idError).innerText = "";
      return true;
    } else {
      document.getElementById(idError).innerText = message;
      document.getElementById(idError).style.display = "block";
      return false;
    }
  },
};
